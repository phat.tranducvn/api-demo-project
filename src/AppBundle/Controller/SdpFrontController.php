<?php
namespace AppBundle\Controller;

use AppBundle\Action\SdpFront\FrontSdpHomeAction;
use AppBundle\Action\SdpFront\FrontSdpSubjectAction;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SdpFrontController extends Controller
{

    /**
     * @Route("/index",name ="index")
     * @Route("/index/",name ="index")
     */
    public function homeAction(Request $request)
    {
        $instance = new FrontSdpHomeAction();
        return $instance->homeAction($this);
    }

    /**
     * @Route("/subjects",name ="list_subjects")
     * @Route("/subjects/",name ="list_subjects")
     */
    public function listSubjectsAction(Request $request)
    {
        $instance = new FrontSdpSubjectAction();
        return $instance->getList($this);
    }

    /**
     * @Route("/subject/{id}",name ="edit_subject")
     * @Route("/subject/{id}/",name ="edit_subject")
     */
    public function editSubjectsAction(Request $request,$id)
    {
        $instance = new FrontSdpSubjectAction();
        return $instance->editSubject($this,$request,$id);
    }

}