<?php
namespace AppBundle\Controller;
;

use AppBundle\Action\SdpApi\ApiSdpClasses;
use AppBundle\Action\SdpApi\ApiSdpStudent;
use AppBundle\Action\SdpApi\ApiSdpSubject;
use AppBundle\Action\SdpApi\ApiSdpTeacher;
use FOS\RestBundle\Controller\Annotations\Get ;
use FOS\RestBundle\Controller\Annotations\Post ;
use FOS\RestBundle\Controller\Annotations\Put ;
use FOS\RestBundle\Controller\Annotations\Delete ;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

class SdpApiController extends FOSRestController
{
    /**
     *  Subject
     */

    /**
     * @return array
     * @GET("/api/subjects/",name = "api_subjects")
     * @GET("/api/subjects")
     */
    public function getListSubjectAction()
    {
        $instance = new ApiSdpSubject();

        return $instance->getListSubjectAction($this);
    }
    /**
     * @param Request $request
     * @return array
     * @GET("/api/subject/{id}")
     * @GET("/api/subject/{id}/")
     */
    public function getSubjectAction($id)
    {
        $instance = new ApiSdpSubject();

        return $instance->getSubjectAction($this,$id);
    }

    /**
     * @param Request $request
     * @return array
     * @GET("/api/subject/{id}/listteacher")
     * @GET("/api/subject/{id}/listteacher")
     */
    public function getListTeacherAction($id)
    {
        $instance = new ApiSdpSubject();

        return $instance->getListTeacherAction($this,$id);
    }

    /**
     * @param Request $request
     * @return array
     * @POST("/api/subject/")
     * @POST("/api/subject")
     */
    public function addSubjectAction(Request $request)
    {
        $instance = new ApiSdpSubject();

        return $instance->addSubjectAction($this,$request);
    }
    /**
     * @param Request $request
     * @return array
     * @PUT("/api/subject/{id}")
     * @PUT("/api/subject/{id}/")
     */
    public function updateSubjectAction(Request $request,$id)
    {
        $instance = new ApiSdpSubject();

        return $instance->updateSubjectAction($this,$request,$id);
    }

    /**
     * @param Request $request
     * @return array
     * @DELETE("/api/subject/{id}")
     * @DELETE("/api/subject/{id}/")
     */
    public function deleteSubjectAction(Request $request,$id)
    {
        $instance = new ApiSdpSubject();

        return $instance->deleteSubjectAction($this,$request,$id);
    }

    /**
     *  Teacher
     */

    /**
     * @param Request $request
     * @return array
     * @GET("/api/teachers/")
     * @GET("/api/teachers")
     */
    public function getSubjectListTeacherAction()
    {
        $instance = new ApiSdpTeacher();

        return $instance->getListTeacherAction($this);
    }
    /**
     * @param Request $request
     * @return array
     * @GET("/api/teacher/{id}")
     * @GET("/api/teacher/{id}/")
     */
    public function getTeacherAction($id)
    {
        $instance = new ApiSdpTeacher();

        return $instance->getTeacherAction($this,$id);
    }

    /**
     * @param Request $request
     * @return array
     * @POST("/api/teacher/")
     * @POST("/api/teacher")
     */
    public function addTeacherAction(Request $request)
    {
        $instance = new ApiSdpTeacher();

        return $instance->addTeacherAction($this,$request);
    }
    /**
     * @param Request $request
     * @return array
     * @PUT("/api/teacher/{id}")
     * @PUT("/api/teacher/{id}/")
     */
    public function updateTeacherAction(Request $request,$id)
    {
        $instance = new ApiSdpTeacher();

        return $instance->updateTeacherAction($this,$request,$id);
    }

    /**
     * @param Request $request
     * @return array
     * @DELETE("/api/teacher/{id}")
     * @DELETE("/api/teacher/{id}/")
     */
    public function deleteTeacherAction(Request $request,$id)
    {
        $instance = new ApiSdpTeacher();

        return $instance->deleteTeacherAction($this,$request,$id);
    }

    /**
     *  Classes
     */

    /**
     * @param Request $request
     * @return array
     * @GET("/api/classes/")
     * @GET("/api/classes")
     */
    public function getListClassesAction()
    {
        $instance = new ApiSdpClasses();

        return $instance->getListClassesAction($this);
    }
    /**
     * @param Request $request
     * @return array
     * @GET("/api/classes/{id}")
     * @GET("/api/classes/{id}/")
     */
    public function getClassesAction($id)
    {
        $instance = new ApiSdpClasses();

        return $instance->getClassesAction($this,$id);
    }

    /**
     * @param Request $request
     * @return array
     * @GET("/api/classes/{id}/liststudent")
     * @GET("/api/classes/{id}/liststudent/")
     */
    public function getClassListStudentAction($id)
    {
        $instance = new ApiSdpClasses();

        return $instance->getListStudentAction($this,$id);
    }

    /**
     * @param Request $request
     * @return array
     * @POST("/api/classes/")
     * @POST("/api/classes")
     */
    public function addClassesAction(Request $request)
    {
        $instance = new ApiSdpClasses();

        return $instance->addClassesAction($this,$request);
    }
    /**
     * @param Request $request
     * @return array
     * @PUT("/api/classes/{id}")
     * @PUT("/api/classes/{id}/")
     */
    public function updateClassesAction(Request $request,$id)
    {
        $instance = new ApiSdpClasses();

        return $instance->updateClassesAction($this,$request,$id);
    }

    /**
     * @param Request $request
     * @return array
     * @DELETE("/api/classes/{id}")
     * @DELETE("/api/classes/{id}/")
     */
    public function deleteClassesAction(Request $request,$id)
    {
        $instance = new ApiSdpClasses();

        return $instance->deleteClassesAction($this,$request,$id);
    }

    /**
     *  Student
     */

    /**
     * @param Request $request
     * @return array
     * @GET("/api/students/")
     * @GET("/api/students")
     */
    public function getListStudentAction()
    {
        $instance = new ApiSdpStudent();

        return $instance->getListStudentAction($this);
    }
    /**
     * @param Request $request
     * @return array
     * @GET("/api/student/{id}")
     * @GET("/api/student/{id}/")
     */
    public function getStudentAction($id)
    {
        $instance = new ApiSdpStudent();

        return $instance->getStudentAction($this,$id);
    }


    /**
     * @param Request $request
     * @return array
     * @POST("/api/student/")
     * @POST("/api/student")
     */
    public function addStudentAction(Request $request)
    {
        $instance = new ApiSdpStudent();

        return $instance->addStudentAction($this,$request);
    }
    /**
     * @param Request $request
     * @return array
     * @PUT("/api/student/{id}")
     * @PUT("/api/student/{id}/")
     */
    public function updateStudentAction(Request $request,$id)
    {
        $instance = new ApiSdpStudent();

        return $instance->updateStudentAction($this,$request,$id);
    }

    /**
     * @param Request $request
     * @return array
     * @DELETE("/api/student/{id}")
     * @DELETE("/api/student/{id}/")
     */
    public function deleteStudentAction(Request $request,$id)
    {
        $instance = new ApiSdpSubject();

        return $instance->deleteStudentAction($this,$request,$id);
    }

}