<?php
namespace AppBundle\Action\SdpApi;

use AppBundle\Entity\Subject;
use AppBundle\Entity\Teacher;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\Date;

class ApiSdpTeacher extends Controller
{
    public function getListTeacherAction(Controller $controller)
    {
        $entityManager = $controller->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Teacher::class);
        return $repository->findBy(array('status' => 'Active'));
    }

    public function getTeacherAction(Controller $controller,$id)
    {
        $entityManager = $controller->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Teacher::class);

        return $repository->findOneBy(array('id' => $id,'status' => 'Active'));
    }

    public function addTeacherAction(Controller $controller,$request)
    {
        $entityManager = $controller->getDoctrine()->getManager();
        //content
        $content = array();

        if(!empty($request->get('name')) && !empty($request->get('phone')) && !empty($request->get('id_subject')))
        {
                if(!is_numeric($request->get('id_subject')))
                {
                    $content['code'] = 400;
                    $content['message'] = 'id_subject phải là số.';
                    return $content;
                }elseif(!$controller->getDoctrine()->getRepository(Subject::class)->find($request->get('id_subject')))
                {
                    $content['code'] = 400;
                    $content['message'] = 'id_subject không tồn tại.';
                    return $content;
                }


            $teacher = new Teacher();
            $teacher->setName($request->get('name'));
            $teacher->setStatus('Active');
            $teacher->setPhone($request->get('phone'));
            $teacher->setIdSubject($request->get('id_subject'));
                $entityManager->persist($teacher);
                $entityManager->flush();

                $content['code'] = 200;
                $content['message'] = 'Thêm mới thành công';
                return $content;

        }else{
            $content['code'] = 400;
            $content['message'] = 'Không đủ trường thông tin để tạo mới';
            return $content;
        }

    }

    public function updateTeacherAction(Controller $controller,$request,$id)
    {
        $entityManager = $controller->getDoctrine()->getManager();

        $repository = $controller->getDoctrine()->getRepository(Teacher::class);
        //content
        $content = array();

        //Teacher
        $teacher = new Teacher();
        $teacher = $repository->findOneBy(array('id'=>$id));

        if($teacher)
        {
            if(empty($request->get('name')) && empty($request->get('phone')) && empty($request->get('id_subject')))
            {
                $content['code'] = 400;
                $content['message'] = 'Không có thông tin nào được thay đổi';

                return $content;
            }
            if(!empty($request->get('name')))
            {
                $teacher->setName($request->get('name'));
            }
            if(!empty($request->get('phone')))
            {
                $teacher->setPhone($request->get('phone'));
            }
            if(!empty($request->get('id_subject')))
            {
                if ($entityManager->getRepository(Subject::class)->find($request->get('id_subject')))
                {
                    $teacher->setIdSubject($request->get('id_subject'));

                } else {
                    $content['code'] = 400;
                    $content['message'] = 'id_subject không hợp lệ';

                    return $content;
                }
            }

            $entityManager->flush();
            $content['code'] = 200;
            $content['message'] = 'Cập nhật thành công';
            return $content;
        }
        else {
            $content['code'] = 400;
            $content['message'] = 'ID không hợp lệ';

            return $content;
        }
    }

    public function deleteTeacherAction(Controller $controller,$request,$id)
    {
        $entityManager = $controller->getDoctrine()->getManager();

        $repository = $controller->getDoctrine()->getRepository(Teacher::class);
        //content
        $content = array();
        //Teacher
        $teacher = new Teacher();
        $teacher = $repository->findOneBy(array('id'=>$id));

        if($teacher)
        {
            $teacher->setStatus('Inactive');

            $entityManager->flush();

            $content['code'] = 200;
            $content['message'] = 'Xóa thành công';
            return $content;
        }else{
            $content['code'] = 400;
            $content['message'] = 'ID không hợp lệ';
            return $content;
        }

    }
}