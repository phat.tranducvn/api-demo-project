<?php
namespace AppBundle\Action\SdpApi;

use AppBundle\Entity\Classes;
use AppBundle\Entity\Subject;
use AppBundle\Entity\Student;
use AppBundle\Entity\Teacher;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

class ApiSdpStudent extends Controller
{
    public function getListStudentAction(Controller $controller)
    {
        $entityManager = $controller->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Student::class);
        $student = $repository->findBy(array('status' => 'Active'));

        return $controller->container->get('jms_serializer')
            ->serialize($student, 'json');
    }

    public function getStudentAction(Controller $controller,$id)
    {
        $entityManager = $controller->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Student::class);

        $student = $repository->findOneBy(array('id' => $id,'status' => 'Active'));
        return $controller->container->get('jms_serializer')
            ->serialize($student, 'json');
    }

    public function addStudentAction(Controller $controller,$request)
    {
        $entityManager = $controller->getDoctrine()->getManager();
        //content
        $content = array();

        if(!empty($request->get('name')) && !empty($request->get('phone')) && !empty($request->get('id_class')))
        {
                
                if(!is_numeric($request->get('id_class')))
                {
                    $content['code'] = 400;
                    $content['message'] = 'id_class phải là số.';
                    return $content;
                }elseif(!$controller->getDoctrine()->getRepository(Subject::class)->find($request->get('id_class')))
                {
                    $content['code'] = 400;
                    $content['message'] = 'id_class không tồn tại.';
                    return $content;
                }

                
                $student = new Student();
                $student->setName($request->get('name'));
                $student->setStatus('Active');
                $student->setPhone($request->get('phone'));
                $student->setIdClass($request->get('id_class'));

                $entityManager->persist($student);
                $entityManager->flush();

                $content['code'] = 200;
                $content['message'] = 'Thêm mới thành công';
                return $content;

        }else{
            $content['code'] = 400;
            $content['message'] = 'Không đủ trường thông tin để tạo mới';
            return $content;
        }

    }

    public function updateStudentAction(Controller $controller,$request,$id)
    {
        $entityManager = $controller->getDoctrine()->getManager();

        $repository = $controller->getDoctrine()->getRepository(Student::class);
        //content
        $content = array();

        //Student
        $student = new Student();
        $student = $repository->findOneBy(array('id'=>$id));

        if($student)
        {
            if(empty($request->get('name')) && empty($request->get('phone')) && empty($request->get('id_class')))
            {
                $content['code'] = 400;
                $content['message'] = 'Không có thông tin nào được thay đổi';

                return $content;
            }

            if(!empty($request->get('name')))
            {
                $student->setName($request->get('name'));
            }
            if(!empty($request->get('phone')))
            {
                $student->setPhone($request->get('phone'));
            }
            if(!empty($request->get('id_class')))
            {
                if(!is_numeric($request->get('id_class')))
                {
                    $content['code'] = 400;
                    $content['message'] = 'id_class phải là số.';
                    return $content;
                }elseif(!$controller->getDoctrine()->getRepository(Subject::class)->find($request->get('id_class')))
                {
                    $content['code'] = 400;
                    $content['message'] = 'id_class không tồn tại.';
                    return $content;
                }else{
                    $student->setIdClass($request->get('id_class'));
                }

            }

            $entityManager->flush();
            $content['code'] = 200;
            $content['message'] = 'Cập nhật thành công';
            return $content;
        }
        else {
            $content['code'] = 400;
            $content['message'] = 'ID không hợp lệ';

            return $content;
        }
    }

    public function deleteStudentAction(Controller $controller,$request,$id)
    {
        $entityManager = $controller->getDoctrine()->getManager();

        $repository = $controller->getDoctrine()->getRepository(Student::class);
        //content
        $content = array();
        //Student
        $student = new Student();
        $student = $repository->findOneBy(array('id'=>$id));

        if($student)
        {
            $student->setStatus('Inactive');

            $entityManager->flush();

            $content['code'] = 200;
            $content['message'] = 'Xóa thành công';
            return $content;
        }else{
            $content['code'] = 400;
            $content['message'] = 'ID không hợp lệ';
            return $content;
        }

    }

}