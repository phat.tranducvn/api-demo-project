<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/04/2018
 * Time: 3:47 CH
 */

namespace AppBundle\Action\SdpFront;


use AppBundle\Action\SdpApi\ApiSdpSubject;
use AppBundle\Entity\Subject;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;

class FrontSdpSubjectAction extends Controller
{

    public function getList(Controller $controller)
    {
        $listSubject = new ApiSdpSubject();
        $listSubject = $listSubject->getListSubjectAction($controller);
        $subjects = array();

        foreach ($listSubject as $value) {
            $tmp = [];
            $tmp['id'] = $value->getId();
            $tmp['name'] = $value->getName();
            $tmp['status'] = $value->getStatus();
            $subjects[] = $tmp;
        }

        return $controller->render(
            '@front/sdp-front/front-sdp-subject-list.html.twig',
            [
                'subjects' => $subjects,
            ]
        );
    }

    public function editSubject(Controller $controller, $request, $id)
    {
        $result = array();
        $data = array();
        //form
        $form = $this->buildForm($controller);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $data['id'] = $id;
            $instance_subject = new ApiSdpSubject();
            $result = $instance_subject->addSubjectAction($controller, $data);
            print_r($result);
            die;
        }

//
//        return $controller->render(
//                '@front/sdp-front/front-sdp-subject-add.html.twig',
//                [
//                    'form' => $form->createView(),
//                ]
//            );
//        } elseif ($controller->getDoctrine()->getRepository(Subject::class)->find($id)) {
//
//            return $controller->render(
//                '@front/sdp-front/front-sdp-subject-add.html.twig',
//                [
//                    'form' => $form->createView(),
//                ]
//            );
//        }

        return $controller->render(
                '@front/sdp-front/front-sdp-subject-add.html.twig',
                [
                    'form' => $form->createView(),
                ]
            );
    }

    public function buildForm(Controller $controller)
    {
        $form = $controller->createFormBuilder()
            ->add(
                'name',
                TextType::class,
                array(
                    'attr' => array('label' => 'Name'),
                )
            )
            ->add(
                'add',
                SubmitType::class,
                array(
                    'attr' => array('label' => 'Add'),
                )
            )
            ->getForm();

        return $form;
    }
}