<?php
namespace AppBundle\Action\SdpApi;

use AppBundle\Entity\Subject;
use AppBundle\Entity\Teacher;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ApiSdpSubject extends Controller
{
    public function getListSubjectAction(Controller $controller)
    {
        $entityManager = $controller->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Subject::class);
        return $repository->findBy(array('status' => 'Active'));
    }

    public function getSubjectAction(Controller $controller,$id)
    {
        $entityManager = $controller->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Subject::class);

        return $repository->findOneBy(array('id' => $id,'status' => 'Active'));
    }

    public function addSubjectAction(Controller $controller,$data)
    {
        $entityManager = $controller->getDoctrine()->getManager();
        //content
        $content = array();
        //info
        $subject = new Subject();
        if($data['id'] == 0)
        {
            $data['name'] ? $subject->setName($data['name']) : $subject->setName('');
            if($subject->getName() != '' && is_string($subject->getName()))
            {
                $subject->setStatus('Active');

                $entityManager->persist($subject);
                $entityManager->flush();

                $content['status'] = 200;
                $content['message'] = 'Thêm mới thành công';
                return $content;
            }
            elseif($subject->getName() == ''){
                $content['status'] = 400;
                $content['message'] = 'Rỗng';
                return $content;
            }
            elseif(!is_string($subject->getName())){
                $content['status'] = 400;
                $content['message'] = 'Không phải string';
                return $content;
            }
        }elseif($data['id'] != 0){
            $subject = $entityManager->getRepository(Subject::class)->find($data['id']);
            if($subject)
            {
                !empty($data['name']) ? $subject->setName($data['name']) : $subject->setName(1);
                if(is_string($subject->getName()))
                {
                    $entityManager->flush();

                    $content['status'] = 200;
                    $content['message'] = 'Cập nhật thành công';
                    return $content;
                }
                else{
                    $content['status'] = 400;
                    $content['message'] = 'Không phải string';
                    return $content;
                }
            }else{
                $content['status'] = 400;
                $content['message'] = 'Id không tồn tại';
                return $content;
            }
        }

    }

    public function updateSubjectAction(Controller $controller,$request,$id)
    {
        $entityManager = $controller->getDoctrine()->getManager();

        $repository = $controller->getDoctrine()->getRepository(Subject::class);
        //content
        $content = array();
        //subject
        $subject = new Subject();
        $subject = $repository->findOneBy(array('id'=>$id));

        if($subject)
        {
            $subject->setName($request->get('name'));

            $entityManager->flush();

            $content['code'] = 200;
            $content['message'] = 'Cập nhật thành công';
            return $content;
        }else{
            $content['code'] = 400;
            $content['message'] = 'ID không hợp lệ';
            return $content;
        }

    }

    public function deleteSubjectAction(Controller $controller,$request,$id)
    {
        $entityManager = $controller->getDoctrine()->getManager();

        $repository = $controller->getDoctrine()->getRepository(Subject::class);
        //content
        $content = array();
        //subject
        $subject = new Subject();
        $subject = $repository->findOneBy(array('id'=>$id));

        if($subject)
        {
            $subject->setStatus('Inactive');

            $entityManager->flush();

            $content['code'] = 200;
            $content['message'] = 'Xóa thành công';
            return $content;
        }else{
            $content['code'] = 400;
            $content['message'] = 'ID không hợp lệ';
            return $content;
        }

    }

    public function getListTeacherAction(Controller $controller,$id)
    {
        $content = array();
        $entityManager = $controller->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Subject::class);
        $subject = $repository->findOneBy(array('status' => 'Active','id' => $id));
        if($subject)
        {
            $teacher = $entityManager->getRepository(Teacher::class)->findBy(array("idSubject" => $subject->getId()));
            $content['total'] = count($teacher);
            $content['list'] = $teacher;
            return $controller->container->get('jms_serializer')
                ->serialize($content, 'json'); 
        }else
            $content['code'] = 400;
        $content['message'] = 'ID không hợp lệ';
        return $content;
    }

}