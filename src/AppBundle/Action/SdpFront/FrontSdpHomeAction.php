<?php
namespace AppBundle\Action\SdpFront;

use AppBundle\Action\SdpApi\ApiSdpSubject;
use AppBundle\Controller\SdpApiController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FrontSdpHomeAction extends Controller
{

    public function homeAction(Controller $controller)
    {
        $listSubject = new ApiSdpSubject();
        $listSubject = $listSubject->getListSubjectAction($controller);
        $subjects = array();
        foreach ($listSubject as $value)
        {
            $tmp = [];
            $tmp['id'] = $value->getId();
            $tmp['name'] = $value->getName();
            $tmp['status'] = $value->getStatus();
            $subjects[] = $tmp;
        }

        return $controller->render('@front/sdp-front/front-sdp-index.html.twig',[

        ]);
    }

}