<?php
namespace AppBundle\Action\SdpApi;

use AppBundle\Entity\Classes;
use AppBundle\Entity\Student;
use AppBundle\Entity\Subject;
use AppBundle\Entity\Teacher;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ApiSdpClasses extends Controller
{
    public function getListClassesAction(Controller $controller)
    {
        $entityManager = $controller->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Classes::class);
        $class = $repository->findBy(array('status' => 'Active'));
        return $controller->container->get('jms_serializer')
            ->serialize($class, 'json');
    }

    public function getClassesAction(Controller $controller,$id)
    {
        $entityManager = $controller->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Classes::class);

        $class = $repository->findOneBy(array('id' => $id,'status' => 'Active'));
        return $controller->container->get('jms_serializer')
            ->serialize($class, 'json');
    }

    public function addClassesAction(Controller $controller,$request)
    {
        $entityManager = $controller->getDoctrine()->getManager();
        //content
        $content = array();

        if(!empty($request->get('name')) && !empty($request->get('id_teacher')))
        {
            if(!is_numeric($request->get('id_teacher')))
            {
                $content['code'] = 400;
                $content['message'] = 'id_teacher phải là số';
                return $content;
            }elseif(!$controller->getDoctrine()->getRepository(Teacher::class)->find($request->get('id_teacher')) )
            {
                $content['code'] = 400;
                $content['message'] = 'id_teacher không tồn tại';
                return $content;
            }
            $classes = new Classes();
            $classes->setName($request->get('name'));
            $classes->setIdTeacher($request->get('id_teacher'));
            $classes->setStatus('Active');

            $entityManager->persist($classes);
            $entityManager->flush();

            $content['code'] = 200;
            $content['message'] = 'Thêm mới thành công';
            return $content;

        }else{
            $content['code'] = 400;
            $content['message'] = 'Không đủ dữ liệu để tạo';
            return $content;
        }

    }

    public function updateClassesAction(Controller $controller,$request,$id)
    {
        $entityManager = $controller->getDoctrine()->getManager();

        $repository = $controller->getDoctrine()->getRepository(Classes::class);
        //content
        $content = array();
        //Classes
        $classes = new Classes();
        $classes = $repository->findOneBy(array('id'=>$id));

        if($classes)
        {
            if(empty($request->get('name')) && empty($request->get('id_teacher')))
            {
                $content['code'] = 400;
                $content['message'] = 'Không có thông tin nào được thay đổi';
                return $content;
            }
            if(!empty($request->get('id_teacher')))
            {
                if(!is_numeric($request->get('id_teacher')))
                {
                    $content['code'] = 400;
                    $content['message'] = 'id_teacher phải là số';
                    return $content;
                }elseif(!$controller->getDoctrine()->getRepository(Teacher::class)->find($request->get('id_teacher')) )
                {
                    $content['code'] = 400;
                    $content['message'] = 'id_teacher không tồn tại';
                    return $content;
                }else{
                    $classes->setIdTeacher($request->get('id_teacher'));
                }
            }

            if(!empty($request->get('name'))){
                $classes->setName($request->get('name'));
            }

            $entityManager->flush();

            $content['code'] = 200;
            $content['message'] = 'Cập nhật thành công';
            return $content;

        }else{
            $content['code'] = 400;
            $content['message'] = 'ID không hợp lệ';
            return $content;
        }

    }

    public function deleteClassesAction(Controller $controller,$request,$id)
    {
        $entityManager = $controller->getDoctrine()->getManager();

        $repository = $controller->getDoctrine()->getRepository(Classes::class);
        //content
        $content = array();
        //Classes
        $classes = new Classes();
        $classes = $repository->findOneBy(array('id'=>$id));

        if($classes)
        {
            $classes->setStatus('Inactive');

            $entityManager->flush();

            $content['code'] = 200;
            $content['message'] = 'Xóa thành công';
            return $content;
        }else{
            $content['code'] = 400;
            $content['message'] = 'ID không hợp lệ';
            return $content;
        }

    }

    public function getListStudentAction(Controller $controller,$id)
    {
        $content = array();
        $entityManager = $controller->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Classes::class);
        $class = $repository->findOneBy(array('status' => 'Active','id' => $id));
        if($class)
        {
            $student = $entityManager->getRepository(Student::class)->findBy(array("idClass" => $class->getId()));
            $content['total'] = count($student);
            $content['list'] = $student;
            return $controller->container->get('jms_serializer')
                ->serialize($content, 'json');
        }else
            $content['code'] = 400;
        $content['message'] = 'ID không hợp lệ';
        return $content;
    }


}